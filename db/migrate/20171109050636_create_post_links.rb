class CreatePostLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :post_links do |t|
      t.belongs_to :link
      t.integer :quantity_click, default: 0

      t.timestamps
    end
  end
end

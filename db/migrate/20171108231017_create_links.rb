class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :full_url, unique: true
      t.string :short_url, unique: true
      t.belongs_to :user

      t.timestamps
    end
  end
end

class PostLinkController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post_link, only: [:update, :destroy]  

  def index
    @posts_link = PostLink.all

    render json: @posts
  end

  def create
    @post_link = PostLink.new(posts_link_params)
    if @post_link.save
      render json: @post_link, status: :created, location: @post_link
    else
      render json: @post_link.errors, status: :unprocessable_entity
    end
  end

  def update
    if @post_link.update(posts_link_params)
      render json: @post_link
    else
      render json: @post_link.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @post_link.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_link
      @posts_link = PostLink.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def posts_link_params
      params.require(:post_link).permit(:link_id, :quantity_click)
    end

  end
class LinksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_link, only: [:update, :destroy]  

  def index
    @links = Link.where(user: current_user)

    render json: @links
  end

  def create
    @link = Link.new(link_params)
    @link.user = current_user
    if @link.save
      render json: @link, status: :created, location: @link
    else
      render json: @link.errors, status: :unprocessable_entity
    end
  end

  def update
    if @link.update(link_params)
      render json: @link
    else
      render json: @link.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @link.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def link_params
      params.require(:link).permit(:full_url, :short_url, :user_id)
    end

end
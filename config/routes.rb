Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  resources :links
  get 'posts_link' => 'post_link#index'
  resources :posts_link , except: [:index]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
